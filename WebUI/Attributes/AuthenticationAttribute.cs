﻿using System.Web.Mvc;
using System.Web.Routing;
using WebUI.Models;

namespace WebUI.Attributes
{
    public class AuthenticationAttribute : AuthorizeAttribute
    {
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (filterContext.ActionDescriptor.GetCustomAttributes(typeof(AllowAnonymousAttribute), false).Length <= 0)
            {
                if (filterContext.HttpContext.Session["CurrentUser"] == null)
                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Account", action = "SignIn" }));
                else
                {
                    var admin = filterContext.HttpContext.Session["CurrentUser"] as Admin;
                    if (admin.Id <= 0 || string.IsNullOrEmpty(admin.Username))
                    {
                        filterContext.HttpContext.Session["CurrentUser"] = null;
                        filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Account", action = "SignIn" }));
                    }
                }
            }
        }
    }
}