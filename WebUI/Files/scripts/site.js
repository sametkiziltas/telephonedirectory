﻿$(document).ready(function () {
    $(".list-item").on("click", function (event) {
        event.preventDefault();
        $(".list-item").removeClass("active");
        $(this).addClass("active");
        $("#title").html($(this).html() + " Detay Bilgisi");
        $("#title").removeClass("hidden");
        var id = $(this).data("id");
        $.ajax({
            url: "/Employee/Detail?id=" + id,
            type: 'GET',
            success: function (response) {
                $('.detail-content').html(response);
                $('.detail').addClass("active");
                setTimeout(function () {
                    BindOperations();
                }, 250);
            }
        });
    });
});

$(document).ajaxSend(function (event, request, settings) {
    $('#loading-indicator').show();
});

$(document).ajaxComplete(function (event, request, settings) {
    $('#loading-indicator').hide();
});

function BindOperations() {
    $(".remove").on("click", function (e) {
        e.preventDefault();
        var url = $(this).attr("href");
        var r = confirm("Silmek istediğinizden emin misiniz?");
        if (r == true) {
            $.ajax({
                url: url,
                type: 'GET',
                success: function (response) {
                    if (response.IsSuccess) {
                        $(".left-panel .list-item.active").remove();
                        $(".detail.active").html("");
                        $(".detail.active").removeClass("active");
                        alert(response.Message);
                    } else {
                        alert(response.Message);
                    }
                }
            });
        }
    });
}