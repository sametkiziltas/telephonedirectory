﻿using System.Data.Entity;
using WebUI.Models;

namespace WebUI.Database
{
    public class TelephoneDirectoryDbContext : DbContext
    {
        public DbSet<Employee> Employee { get; set; }
        public DbSet<Department> Department { get; set; }
        public DbSet<Admin> Admin { get; set; }
    }
}