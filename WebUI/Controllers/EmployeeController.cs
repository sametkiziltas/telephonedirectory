﻿using System.Linq;
using System.Web.Mvc;
using WebUI.Attributes;
using WebUI.Database;
using WebUI.Models;
using WebUI.Models.ViewModels;

namespace WebUI.Controllers
{
    [Authentication]
    public class EmployeeController : Controller
    {
        public TelephoneDirectoryDbContext DbContext { get; }
        public EmployeeController()
        {
            this.DbContext = new TelephoneDirectoryDbContext();
        }
        [AllowAnonymous]
        public ActionResult List()
        {
            var model = new EmployeeListViewModel()
            {
                Employees = DbContext.Employee.Include("Department").ToList()
            };
            return View(model);
        }
        [AllowAnonymous]
        public ActionResult Detail(int id)
        {
            var detailModel = new EmployeeDetailViewModel()
            {
                Employee = DbContext.Employee.Include("Department")
                                             .Include("Manager")
                                             .FirstOrDefault(employee => employee.Id == id)
            };
            return PartialView(detailModel);
        }

        [HttpGet]
        public ActionResult Add()
        {
            var viewModel = new EmployeeAddViewModel()
            {
                Departments = DbContext.Department.ToList(),
                Employees = DbContext.Employee.ToList()
            };
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Add(EmployeeAddViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                if (!this.DbContext.Employee.Any(emp => emp.MobilePhone.Equals(viewModel.Employee.MobilePhone)))
                {
                    this.DbContext.Employee.Add(viewModel.Employee);
                    this.DbContext.SaveChanges();
                    return RedirectToAction("List");
                }
                else
                    ModelState.AddModelError("PostError", "Bu telefon numarası mevcuttur.");
            }
            else
                ModelState.AddModelError("PostError", "Lütfen girdiğiniz bilgileri kontrol ederek tekrar deneyiniz.");

            viewModel.Departments = DbContext.Department.ToList();
            viewModel.Employees = DbContext.Employee.ToList();
            return View("Add", viewModel);
        }

        [Authentication]
        public ActionResult Edit(int id)
        {
            if (id <= 0)
                return RedirectToAction("List");

            var employee = this.DbContext.Employee.FirstOrDefault(e => e.Id == id);
            if (employee == null)
                return RedirectToAction("List");

            var model = new EmployeeEditViewModel
            {
                Employee = employee,
                Departments = DbContext.Department.ToList(),
                Employees = DbContext.Employee.ToList()
            };

            return View(model);
        }

        [Authentication]
        [HttpPost]
        public ActionResult Edit(EmployeeAddViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                if (!this.DbContext.Employee.Any(emp => emp.Id != viewModel.Employee.Id && emp.MobilePhone.Equals(viewModel.Employee.MobilePhone)))
                {
                    var employee = this.DbContext.Employee.FirstOrDefault(e => e.Id == viewModel.Employee.Id);

                    employee.DepartmentID = viewModel.Employee.DepartmentID;
                    employee.ManagerID = viewModel.Employee.ManagerID;
                    employee.MobilePhone = viewModel.Employee.MobilePhone;
                    employee.SecondName = viewModel.Employee.SecondName;
                    employee.Surname = viewModel.Employee.Surname;
                    employee.Name = viewModel.Employee.Name;

                    this.DbContext.SaveChanges();

                    return RedirectToAction("List");
                }
                else
                    ModelState.AddModelError("PostError", "Bu telefon numarası mevcuttur.");
            }
            else
                ModelState.AddModelError("PostError", "Lütfen girdiğiniz bilgileri kontrol ederek tekrar deneyiniz.");

            viewModel.Departments = DbContext.Department.ToList();
            viewModel.Employees = DbContext.Employee.ToList();
            return View("Add", viewModel);
        }

        [Authentication]
        public JsonResult Remove(int id)
        {
            if (id <= 0)
                return Json(new { IsSuccess = false, Message = "Id değeri boş geçilemez." }, JsonRequestBehavior.AllowGet);

            var employee = this.DbContext.Employee.FirstOrDefault(e => e.Id == id);
            if (employee == null)
                return Json(new { IsSuccess = false, Message = "Id değeri boş geçilemez." }, JsonRequestBehavior.AllowGet);

            if (this.DbContext.Employee.Any(e => e.ManagerID == employee.Id))
                return Json(new { IsSuccess = false, Message = "Yönetici olan çalışan silinemez." }, JsonRequestBehavior.AllowGet);

            this.DbContext.Employee.Remove(employee);
            this.DbContext.SaveChanges();
            return Json(new { IsSuccess = true, Message = "Çalışan silindi." }, JsonRequestBehavior.AllowGet);
        }
    }
}