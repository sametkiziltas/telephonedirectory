﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebUI.Attributes;
using WebUI.Database;
using WebUI.Models;
using WebUI.Models.ViewModels;

namespace WebUI.Controllers
{
    [Authentication]
    public class DepartmentController : Controller
    {
        public TelephoneDirectoryDbContext DbContext { get; }
        public DepartmentController()
        {
            this.DbContext = new TelephoneDirectoryDbContext();
        }

        [HttpGet]
        public ActionResult Add()
        {
            return View(new DepartmentAddViewModel());
        }

        [HttpPost]
        public ActionResult Add(DepartmentAddViewModel model)
        {
            if (ModelState.IsValid)
            {
                var entity = model.Department;
                DbContext.Department.Add(entity);
                DbContext.SaveChanges();
                return RedirectToAction("List", "Employee");
            }
            else
                ModelState.AddModelError("PostError", "Departman Ekleme İşlemi Hatalı.");

            return View(model);
        }



    }
}