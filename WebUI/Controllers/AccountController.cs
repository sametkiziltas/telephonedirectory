﻿using System.Web.Mvc;
using WebUI.Attributes;
using WebUI.Database;
using WebUI.Models;
using System.Linq;
using WebUI.Models.ViewModels;

namespace WebUI.Controllers
{
    [Authentication]
    public class AccountController : Controller
    {
        public TelephoneDirectoryDbContext DbContext { get; }
        public AccountController()
        {
            this.DbContext = new TelephoneDirectoryDbContext();
        }

        [AllowAnonymous]
        public ActionResult SignIn() => View(new SignInViewModel());

        [AllowAnonymous]
        [HttpPost]
        public ActionResult SignIn(SignInViewModel model)
        {
            if (ModelState.IsValid)
            {
                var admin = this.DbContext.Admin.FirstOrDefault(a => a.Username.Equals(model.Admin.Username) && a.Password.Equals(model.Admin.Password));
                if (admin == null)
                    ModelState.AddModelError("PostError", "Kullanıcı adı veya şifre hatalıdır. Lütfen tekrar deneyiniz.");
                else
                {
                    model.Admin.Id = admin.Id;
                    model.Admin.Password = "";
                    Session["CurrentUser"] = model.Admin;
                    return RedirectToAction("List", "Employee");
                }
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult ChangePassword() => View(new ChangePasswordViewModel());

        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordViewModel model)
        {
            var result = false;
            if (!ModelState.IsValid)
                ModelState.AddModelError("PostError", "Şifre bilgilerinizi kontrol ederek lütfen tekrar deneyiniz.");
            else
            {
                var currentUser = Session["CurrentUser"] as Admin;
                var admin = this.DbContext.Admin.FirstOrDefault(a => a.Id == currentUser.Id);
                if (!admin.Password.Equals(model.OldPassword))
                    ModelState.AddModelError("PostError", "Eski şifreniz doğru değil. Lütfen tekrar deneyiniz.");
                else
                {
                    admin.Password = model.NewPassword;
                    this.DbContext.SaveChanges();
                    result = true;
                }
            }
            return View(new ChangePasswordViewModel { Result = result });
        }

        public ActionResult SignOut()
        {
            Session["CurrentUser"] = null;
            Session.Clear();
            Session.Abandon();
            return RedirectToAction("List", "Employee");
        }
    }
}