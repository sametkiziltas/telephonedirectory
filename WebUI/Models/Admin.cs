﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace WebUI.Models
{
    public class Admin
    {
        public int Id { get; set; }
        [Display(Name = "Kullanıcı Adı")]
        [Required]
        [MinLength(length: 5, ErrorMessage = "Lütfen 5 karakterden uzun değer giriniz.")]
        [MaxLength(length: 255, ErrorMessage = "Lütfen daha kısa bir kullanıcı adı giriniz.")]
        public string Username { get; set; }
        [Display(Name = "Şifre")]
        [Required]
        [MinLength(length: 5, ErrorMessage = "Lütfen 5 karakterden uzun değer giriniz.")]
        [MaxLength(length: 255, ErrorMessage = "Lütfen daha kısa bir şifre giriniz.")]
        public string Password { get; set; }
    }
}