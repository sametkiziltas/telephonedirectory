﻿using System.Web;

namespace WebUI.Models.Base
{
    public abstract class Model
    {
        public bool IsAuthenticated
        {
            get
            {
                return HttpContext.Current.Session["CurrentUser"] != null;
            }
        }
    }
}