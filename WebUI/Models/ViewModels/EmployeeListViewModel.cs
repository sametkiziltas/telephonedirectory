﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUI.Models.ViewModels
{
    public class EmployeeListViewModel : Base.Model
    {
        public List<Employee> Employees { get; set; }
         
    }
}