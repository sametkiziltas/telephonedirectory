﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUI.Models.ViewModels
{
    public class DepartmentAddViewModel :Base.Model
    {
        public Department Department { get; set; }
    }
}