﻿using System.Collections.Generic;

namespace WebUI.Models.ViewModels
{
    public class EmployeeEditViewModel : Base.Model
    {
        public List<Department> Departments { get; set; }
        public List<Employee> Employees { get; set; }
        public Employee Employee { get; set; }
    }
}