﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebUI.Models.ViewModels
{
    public class ChangePasswordViewModel : Base.Model
    {
        [Required]
        [Display(Name = "Mevcut Şifre")]
        public string OldPassword { get; set; }

        [Required]
        [Display(Name = "Yeni Şifre")]
        public string NewPassword { get; set; }

        [Required]
        [Display(Name = "Yeni Şifre Tekrarı")]
        [Compare("NewPassword", ErrorMessage = "Şifre ve tekrarı uyuşmamaktadır.")]
        public string ConfirmPassword { get; set; }
        public bool Result { get; internal set; }
    }
}