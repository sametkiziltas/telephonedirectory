﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUI.Models.ViewModels
{
    public class EmployeeAddViewModel : Base.Model
    {
        public List<Department> Departments { get; set; }
        public List<Employee> Employees { get; set; }
        public Employee Employee { get; set; }
    }
}