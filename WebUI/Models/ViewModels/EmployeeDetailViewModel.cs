﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUI.Models.ViewModels
{
    public class EmployeeDetailViewModel : Base.Model
    {
        public Employee Employee { get; set; }
    }
}