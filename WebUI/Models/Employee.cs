﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebUI.Models
{
    public class Employee
    {
        public int Id { get; set; }
        [Required]
        [MinLength(length: 3, ErrorMessage = "Lütfen 3 karakterden uzun değer giriniz.")]
        [MaxLength(length: 255, ErrorMessage = "Lütfen daha kısa bir metin giriniz.")]
        [DisplayName("Çalışan Adı")]
        public string Name { get; set; }

        [MinLength(length: 3, ErrorMessage = "Lütfen 3 karakterden uzun değer giriniz.")]
        [MaxLength(length: 255, ErrorMessage = "Lütfen daha kısa bir metin giriniz.")]
        [DisplayName("Çalışan İkinci Adı")]
        public string SecondName { get; set; }

        [Required]
        [MinLength(length: 3, ErrorMessage = "Lütfen 3 karakterden uzun değer giriniz.")]
        [MaxLength(length: 255, ErrorMessage = "Lütfen daha kısa bir metin giriniz.")]
        [DisplayName("Çalışan Soyadı")]
        public string Surname { get; set; }

        [Required]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Lütfen geçerli bir telefon numarası giriniz.")]
        [DisplayName("Çalışan Telefon Numarası")]
        public string MobilePhone { get; set; }
        [DisplayName("Çalışan Yöneticisi")]
        public int? ManagerID { get; set; }
        [DisplayName("Çalışan Departmanı")]
        public int DepartmentID { get; set; } 

        public Department Department { get; set; }
        public Employee Manager { get; set; }
    }
}