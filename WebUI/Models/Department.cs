﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebUI.Models
{
    public class Department
    {
        public int Id { get; set; }
        [Required]
        [MinLength(length: 6, ErrorMessage = "Lütfen 6 karakterden uzun değer giriniz.")]
        [MaxLength(length: 255, ErrorMessage = "Lütfen daha kısa bir metin giriniz.")]
        public string Name { get; set; }
        [MinLength(length: 2, ErrorMessage = "Lütfen 2 karakterden uzun değer giriniz.")]
        [MaxLength(length: 500, ErrorMessage = "Lütfen daha kısa bir metin giriniz.")]
        public string Description { get; set; }
    }
}